/* SPDX-License-Identifier: GPL-2.0 */
extern int tegra_partition_forced_gpt(struct parsed_partitions *state);
extern int tegra_partition(struct parsed_partitions *state);
